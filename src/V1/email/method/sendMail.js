const nodemailer = require('nodemailer') // Importa o módulo principal

class Mail
{
    constructor()
    {
        this.transporter = nodemailer.createTransport({
            host: 'smtp.zoho.com',
            port: 465,
            secure: true,
            auth: {
              user: 'support@nplay.com.br',
              pass: 'Vzfa%v5l'
            }
        })
    }

    async send(from = 'nPlay suporte <support@nplay.com.br>', to, subject, html, attachments = [])
    {
        return new Promise((resolve, reject) => {

            this.transporter.sendMail({
                from: from,
                to: to,
                subject: subject,
                html: html,
                attachments: attachments
            },  (err, info) => {
                if (err)
                    reject(err)
                
                resolve(info)
            })
        })
    }
}

export default Mail