
const fs = require('fs');

const Mustache = require('mustache')

export default async function(content, message, ok, fail){
    try {
        let bufferTemplate = fs.readFileSync(`${__dirname}/../resource/templates/newUser.html`)

        let htmlData = {
            username: content.params.profile.fullName,
        }
        
        let html = Mustache.render(bufferTemplate.toString(), htmlData);

        await this.broker.call('v1.email.send', {
            to: content.params.profile.email,
            subject: `Seja bem vindo ${content.params.profile.fullName}`,
            identifier: null, //Pagina de cadastro não tem identifier
            html: html
        })

        ok(message)
    } catch (error) {
        console.log(error)
        fail(message)
    }
}