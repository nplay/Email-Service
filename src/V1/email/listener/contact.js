
const fs = require('fs');

const Mustache = require('mustache')

export default async function(content, message, ok, fail){
    try {
        let bufferTemplate = fs.readFileSync(`${__dirname}/../resource/templates/contact.html`)

        let htmlData = {
            username: content.params.fullName,
        }
        
        let html = Mustache.render(bufferTemplate.toString(), htmlData);

        await this.broker.call('v1.email.send', {
            to: content.params.email,
            subject: `Olá ${content.params.fullName} recebemos seu contato`,
            identifier: content.params.identifier,
            html: html
        })

        bufferTemplate = fs.readFileSync(`${__dirname}/../resource/templates/internalContact.html`)
        htmlData = content.params

        html = Mustache.render(bufferTemplate.toString(), htmlData);

        await this.broker.call('v1.email.send', {
            to: 'roberto@mystra.com.br;galleger@bis2bis.com.br',
            subject: `Novo contato de ${content.params.fullName}`,
            identifier: content.params.identifier,
            html: html
        })

        ok(message)
    } catch (error) {
        console.log(error)
        fail(message)
    }
}