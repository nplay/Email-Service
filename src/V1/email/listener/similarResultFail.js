
const fs = require('fs');
const Mustache = require('mustache')

class SimilarResultFail
{
    async execute(content, message, ok, fail){
        try {
            let bufferTemplate = fs.readFileSync(`${__dirname}/../resource/templates/devsError.html`)

            let email = 'dogeness@gmail.com'
            let body = content.params
    
            let identifier = body.identifier
            let attachments = body.attachments || []
            let context = body.context || null
            let error = body.error || null
    
           let htmlData = {
                identifier: identifier,
                context: context,
                error: error
            }
    
            let html = Mustache.render(bufferTemplate.toString(), htmlData);
    
            await this.broker.call('v1.email.send', {
                to: email,
                subject: 'Falha - Itens Similares',
                identifier: identifier,
                html: html,
                attachments: attachments
            })
    
            ok(message)
        } catch (error) {
            console.log(error)
            fail(message)
        }
    }

}

export {
    SimilarResultFail
}