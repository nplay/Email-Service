
const fs = require('fs');
import {Config} from "core"

const Mustache = require('mustache')

export default async function(content, message, ok, fail){
    try {
        let bufferTemplate = fs.readFileSync(`${__dirname}/../resource/templates/forgotPassword.html`)

        let htmlData = {
            username: content.params.user.fullName,
            reset_link: `${Config.dashboard.userWritePasswordURL}/${content.params.recoverCode}`
        }
           
        let html = Mustache.render(bufferTemplate.toString(), htmlData);

        await this.broker.call('v1.email.send', {
            to: content.params.email,
            subject: 'Recuperação de senha',
            identifier: content.params.identifier,
            html: html
        })

        ok(message)
    } catch (error) {
        console.log(error)
        fail(message)
    }
}