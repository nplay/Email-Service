
import events2 from 'event2'

import send from './action/send'
import newUser from './listener/newUser'
import userForgotPassword from './listener/userForgotPassword'
import userContact from './listener/contact'
import { SimilarResultFail } from './listener/similarResultFail'
import { FatalError } from './listener/fatalError'

export default {
    name: "email",
    version: 1,
    mixins: [events2],
    actions: {
        send
    },
    events2: [
        {
            event:'v1.user.contact',
            listeners: {
                userContact: {
                    handle: userContact,
                    createDeadLetter: false
                }
            }
        },
        {
            event:'v1.user.forgotPassword',
            listeners: {
                userForgotPassword: {
                    handle: userForgotPassword,
                    createDeadLetter: false
                }
            }
        },
        {
            event:'v1.user.newUserAfter',
            listeners: {
                newUser: {
                    handle: newUser,
                    createDeadLetter: false
                }
            }
        },
        {
            event: 'v1.ipapi.similarResultFail',
            listeners: {
                notifyDevs: {
                    handle: new SimilarResultFail().execute,
                    createDeadLetter: false
                }
            }
        },
        {
            event: 'v1.any.fatalError',
            listeners: {
                notifyDevs: {
                    handle: new FatalError().execute,
                    createDeadLetter: false
                }
            }
        }
    ]
}