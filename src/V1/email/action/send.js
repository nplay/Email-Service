
import SendMailMethod from '../method/sendMail'

export default {
    params: {
        to: { type: "string" },
        html: { type: "string"}
    },

    async handler(ctx) {
        let params = ctx.params

        let mail = new SendMailMethod()

        let from = params.from || undefined
        let attachments = ctx.params.attachments || undefined

        let response = await mail.send(from, params.to, params.subject, params.html, attachments)

        return true
    }
}